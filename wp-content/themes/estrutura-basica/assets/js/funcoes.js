$(document).ready(function() {

    if (!document.addEventListener) {
        window.location = 'http://www.arealocal.com.br/ie6/oculta-site-ie6.php'
    }

    var templateUrl = al_url['templateUrl']
    var homeUrl = al_url['homeUrl']

    /**
     * Form página "central-do-cliente"
     */
    $('input#cpf').prop('placeholder', 'CPF');
    $('input#user_pass').prop('placeholder', 'Senha');

    /**
     * Carrega mais postagens (page-blog)
     */
    $('.blog .mais a').on('click', function(event) {
        event.preventDefault()

        const link = $(this).attr('href')
        const text_load = $('.blog .mais a').html()
        $('.blog .mais a').html('Carregando...')

        $.get(link, function(data) {
            const post = $('.blog >div >article ', data)

            $('.blog >div:not(.mais)').append(post)
            if ($('.blog .mais a', data).attr('href')) {
                $('.blog .mais a').attr('href', $('.blog .mais a', data).attr('href'))
                $('.blog .mais a').html(text_load)
            } else {
                $('.blog .mais').remove()
            }
        })
    })


    /**
     * Carrega mais postagens (page-blog)
     */
    $('#empreendimentos .mais a').on('click', function(event) {
        event.preventDefault()

        const link = $(this).attr('href')
        const text_load = $('#empreendimentos .mais a').html()
        $('#empreendimentos .mais a').html('Carregando...')

        $.get(link, function(data) {
            const post = $('#empreendimentos >.right >.article-container', data)

            $('#empreendimentos >.right').append(post)
            if ($('#empreendimentos .mais a', data).attr('href')) {
                $('#empreendimentos .mais a').attr('href', $('#empreendimentos .mais a', data).attr('href'))
                $('#empreendimentos .mais a').html(text_load)
            } else {
                $('#empreendimentos .mais').remove()
            }
        })
    })

    /**
     * Inputs formulários
     */
    $('input#telefone').mask('(00) 0000-00009')
    $('input#cpf, input[name="cpf"]').mask('000.000.000-00', { reverse: true })

    /**
     * Número chamado (formulário page pós-venda)
     */
    $('div#wpcf7-f297-p296-o1 form').ready(function() {
        $('input#numero_chamado').val(Date.now())
    })

    // SLIDER EMPREENDIMENTO HOME
    $('#sliderCard').lightSlider({
        item: 3,
        gallery: true,
        pager: false,
        slideMargin: 50,
        onSliderLoad: function() {
            $('#sliderCard').removeClass('cS-hidden');
        },
        responsive : [
            {
                breakpoint: 800,
                settings: {
                    item: 1,
                    slideMove: 1,
                    slideMargin: 6,
                }
            }
        ]
    });

    // SLIDER EMPREENDIMENTO HOME
    $('#ultimas_do_blog').lightSlider({
        item: 1,
        auto: true,
        speed: 1200,
        pause: 8000,
        controls: false,
        slideMargin: 50,
        onSliderLoad: function() {
            $('#ultimas_do_blog').removeClass('cS-hidden');
        }
    });

    

    //CONTADOR DINAMICO NA HOME
    if (window.matchMedia('screen and (min-width: 960px)').matches){
        $(document).scroll(function() {
            if($(window).scrollTop() > 1000){
                counter()
            }
        })
    } else {
        counter()
    }

    /** BUSCA HEADER POR CIDADE */
    $('.main-menu form.cidade').submit(function(event) {
        event.preventDefault()
        const valor = $('form.cidade select').val();
        valor
            ? document.location.href = valor
            : document.location.href = (document.origin === undefined ? 'https://melchiorettosandri.com.br' : document.origin) + '/empreendimentos'
    })

    /** BUSCA MENU EMPREENDIMENTOS POR CIDADE */
    $('.menu > select').change(function() {
        const valor = $(this).val();
        valor
            ? document.location.href = valor
            : document.location.href = document.origin + '/empreendimentos'
    })


    /**
     * Página single empreendimento
     */
    $('body.single-empreendimento').ready(function() {

        /**
         * Barras de progresso (single empreendimento)
         */
        $('.status div[id^="circle-"]').each(function() {
            const classe = $(this).find('input').attr('class')

            $('#circle-' + classe).circleProgress({
                value: $('.' + classe).val() === '100' ? 1 : Number($('.' + classe).val()) / 100,
                size: 120,
                fill: {
                    color: '#308300'
                },
                animation: {
                    duration: 2200,
                    easing: 'circleProgressEasing'
                }
            })
        })

        /**
         * Light slider single empreendimento 01
         */
        $('#single-empreendimento-slider-01').lightSlider({
            gallery:true,
            item:1,
            loop:false,
            thumbItem:5,
            slideMargin:0,
            enableDrag: true,
            currentPagerPosition:'left',
            thumbMargin :20,
            thumbnail:true,
            enableThumbDrag :true,
            enableThumbSwipe :true,
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        thumbItem: 2,
                    }
                },
            ],
            onSliderLoad: function(el) {
                el.lightGallery({});
            },
        })

        /**
         * Light slider plantas
         */
        $('#single-empreendimento-slider-plantas').lightSlider({
            item: 1,
            loop: false,
            slideMargin: 0,
            keyPress: true,
            pager: false,
            controls: true,
            speed: 500,
            auto: false,
            currentPagerPosition: 'left',
            responsive: [
                {
                    breakpoint: 480,
                    settings: {
                        slideMargin: 30,
                    }
                },
                {
                    breakpoint: 375,
                    settings: {
                        slideMargin: 60,
                    }
                },
            ],
            onSliderLoad: function() {
                $('#single-empreendimento-slider-plantas').removeClass('cS-hidden');
            }
        })

        //RENDER GOOGLE MAPS API
        const latitude  =  document.querySelector('.lat').value ? document.querySelector('.lat').value : null;
        const longitude =  document.querySelector('.lng').value ? document.querySelector('.lng').value : null;
        maps(latitude,longitude)

        function maps(lat,lng)
        {
            var templateUrl = al_url['templateUrl'];
            const marker_url = templateUrl + '/assets/images/cd-icon-location.png',
                map_zoom = 14

            const main_color = '#2d313f',
            saturation_value= -20,
            brightness_value= 5;

            //set google map options
            const map_options = {
                center: new google.maps.LatLng(lat, lng),
                zoom: map_zoom,
                panControl: false,
                zoomControl: false,
                mapTypeControl: false,
                streetViewControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                // styles: style,
            }
            //inizialize the map
            const map = new google.maps.Map(document.querySelector('#google-container'), map_options);
            //add a custom marker to the map
            const marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                map: map,
                visible: true,
                icon: marker_url,
            });

            //add custom buttons for the zoom-in/zoom-out on the map
            function CustomZoomControl(controlDiv, map) {
                //grap the zoom elements from the DOM and insert them in the map
                var controlUIzoomIn= document.getElementById('cd-zoom-in'),
                    controlUIzoomOut= document.getElementById('cd-zoom-out');
                controlDiv.appendChild(controlUIzoomIn);
                controlDiv.appendChild(controlUIzoomOut);

                // Setup the click event listeners and zoom-in or out according to the clicked element
                google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
                    map.setZoom(map.getZoom()+1)
                });
                google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
                    map.setZoom(map.getZoom()-1)
                });
            }

            var zoomControlDiv = document.createElement('div');
            var zoomControl = new CustomZoomControl(zoomControlDiv, map);

            //insert the zoom div on the top left of the map
            map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);

        }
    })

})

function counter()
{
    $('.count').each( function(){
        const $this = $(this),
        countTo = $this.attr('data-count')

        $({ countNum: $this.text()}).animate({
            countNum: countTo
        },

        {
            duration: 1500,
            easing:'linear',
            step: function() {
            $this.text(Math.floor(this.countNum));
            },
            complete: function() {
                $this.text(this.countNum);
            }
        })
    })
}
