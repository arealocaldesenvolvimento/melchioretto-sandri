<?php
/*******************************************************
 * Includes
 *******************************************************/
include_once('scripts/post-types.php');
include_once('scripts/shortcodes.php');

/*******************************************************
 * Actions e filters
 *******************************************************/
add_action('after_setup_theme', 'al_setup');
add_action( 'init', 'disable_wp_emojicons' );
add_action('show_admin_bar', '__return_false');
add_action('admin_head', 'show_panel_favicon');
add_action('widgets_init', 'add_widget_areas');
add_action('admin_enqueue_scripts', 'admin_scrips');
add_action('login_enqueue_scripts', 'change_login_form_logo');

remove_action('wp_head', 'wp_generator');

add_filter('widget_text', 'do_shortcode');
add_filter('login_errors', 'wrong_login');
add_filter('excerpt_more', 'change_excerpt_more');
add_filter('wp_page_menu_args', 'show_home_menu');
add_filter('login_headerurl', 'login_form_logo_url');
add_filter('excerpt_length', 'change_excerpt_length');
add_filter('sanitize_file_name', 'sanitize_filename', 10);
add_filter('admin_footer_text', 'change_panel_footer_text');
add_filter('login_headertext', 'login_form_logo_url_title');
add_filter('retrieve_password_message', 'reset_password_message', null, 2);
add_filter('wp_mail_content_type', 'wp_mail_return_texthtml');

/*******************************************************
 * Funções de configuração
 *******************************************************/

 function wp_mail_return_texthtml(){
    return "text/html";
 }
/**
 * Troca o texto no rodapé do painel
 */
function change_panel_footer_text() {
    echo '&copy; <a href="http://www.arealocal.com.br/" target="_blank">&Aacute;rea Local</a> - Web especializada';
}

/**
 * Adiciona favicon no painel
 */
function show_panel_favicon() {
    echo '<link href="'.get_image_url('favicon-panel.ico').'" rel="icon" type="image/x-icon">';
}

/**
 * Altera a logo do formulário de login
 */
function change_login_form_logo() {
    echo '<style>.login h1 a{background-image:url('.get_image_url('logo-login-form.png').')!important;}</style>';
}

/**
 * Altera a url da logo do formulário de login
 * @return string
 */
function login_form_logo_url() {
    return get_home_url();
}

/**
 * Altera title da logo do formulário de login
 * @return string
 */
function login_form_logo_url_title() {
    return get_bloginfo('name').' - Desenvolvido por Área Local';
}

/**
 * Adiciona navegação principal, suporte ao html5 e post thumbnail
 */
function al_setup() {
    register_nav_menus(array(
        'principal' => 'Navegação Principal'
    ));

    add_theme_support('post-thumbnails');
    add_theme_support('html5', array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption'));
}

/** Adiciona a home no menu */
function show_home_menu($args)  {
    $args['show_home'] = true;
    return $args;
}

/** Retorna o caminho completo da imagem */
function get_image_url($file = '') {
    return get_template_directory_uri().'/assets/images/'.$file;
}

/** Printa a imagem da logo, se for a home coloca um wrap de h1 */
function get_logo($img = 'logo.png') {
    $blogName = get_bloginfo('name');
    $tag = '<a href="%s" title="%s - %s" id="header-logo"><img src="%s" alt="%s"></a>';
    $tag = ($tag) ? "<h1 id='wrap-logo'>{$tag}</h1>" : '';
    $tag = sprintf($tag, home_url('/'), $blogName, get_bloginfo('description'), get_image_url($img), $blogName);

    echo $tag;
}

/** Registra áreas de widget */
function add_widget_areas() {
    register_sidebar(array(
        'name' => 'Área de Widget Primária',
        'id' => 'area-widget-primaria',
        'description' => 'Área de Widget Primária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => 'Área de Widget Secundária',
        'id' => 'area-widget-secundaria',
        'description' => 'Área de Widget Secundária',
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
}

/**
 * Desabilita os emojis do wordpress
 */
function disable_wp_emojicons() {
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
}

/**
 * Corrige bug de upload de arquivos com acentuação
 * @param $filename string nome do arquivo original
 * @return string nome do arquivo higienizado
 */
function sanitize_filename($filename) {
    $ext = explode('.', $filename);
    $ext = end($ext);
    $sanitized = preg_replace('/[^a-zA-Z0-9-_.]/', '', substr($filename, 0, -(strlen($ext) + 1)));
    $sanitized = str_replace('.', '-', $sanitized);

    if (function_exists('sanitize_title')) {
        $sanitized = sanitize_title($sanitized);
    }

    return strtolower($sanitized.'.'.$ext);
}

/**
 * Altera o tamanho padrão do excerpt em palavras
 * @param $length
 * @return int
 */
function change_excerpt_length($length) {
    return 20;
}

/**
 * Muda tag do excerpt
 * @return string
 */
function change_excerpt_more() {
    return '<a title="' . get_the_title() . '" href="' . get_permalink() . '" class="more-link">Ler mais</a>';
}

/**
 * Padroniza mensagem de erro de login,
 * para não mostrar quando existe o usuário
 * @return string
 */
function wrong_login() {
    return '<b>ERRO</b>: Usuário ou senha incorretos.';
}

/**
 * Sobre escreve mensagem enviada por e-mail ao usário que quer recuperar a senha
 * a padrão do wordpress não envia o link para alteração
 * @param $message
 * @param $key
 * @return string
 */
function reset_password_message($message, $key) {
    if (strpos($_POST['user_login'], '@')) {
        $user_data = get_user_by('email', trim($_POST['user_login']));
    } else {
        $login = trim($_POST['user_login']);
        $user_data = get_user_by('login', $login);
    }

    $user_login = $user_data->user_login;

    $msg = 'Recebemos um pedido de troca de senha para a conta abaixo em nosso site ('.get_bloginfo('name').'):<br /><br />';
    $msg .= 'Usuário: '.$user_login.'<br />';
    $msg .= 'Se você não solicitou, apenas ignore este e-mail.<br />';
    $msg .= 'Para trocar sua senha, acesse a seguinte url: '.home_url("wp-login.php?action=rp&key=$key&login=".rawurlencode($user_login), 'login');

    return $msg;
}

//Adiciona scrip admin.js e admin.css na tela do admin
function admin_scrips() {
    wp_enqueue_style('admin-styles', get_bloginfo("template_url").'/assets/css/admin.css');
    wp_enqueue_script('admin-functions', get_bloginfo("template_url").'/assets/js/admin.js');
}

//Traz a thumbnail, imagem do post ou img padrão
function get_thumbnail_url($post_id, $size){
    if( !isset($post_id)) {
        $post_id = get_the_ID();
    }
    if( has_post_thumbnail( $post_id ) ) {
        $post_thumbnail_url = get_the_post_thumbnail_url($post_thumbnail_id = '', $size);
    } else {
        $args = array(
            'numberposts' => 1,
            'order' => 'ASC',
            'post_mime_type' => 'image',
            'post_parent' => $post_id,
            'post_status' => null,
            'post_type' => 'attachment',
        );
        $post_thumbnail_url = array_values(get_children($args))[0]->guid;

        if (empty($post_thumbnail_url)) {
            $post_thumbnail_url = get_image_url('default.png');
        }
    }
    return $post_thumbnail_url;
}

/**
 * Redireciona um Catedory page ou Taxonomy page para uma archive-page ou page
 */
function template_redirect($template) {
    if (is_tax('status') || is_tax('cidade')) {
        $template = get_query_template('page-empreendimentos');
    }
    return $template;
}
add_filter('template_include', 'template_redirect');

/**
 * Google API
 */
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyAtB3tWv4XW89UzZjX5DhHYKfpvbaxDfMc');
}
add_action('acf/init', 'my_acf_init');

/**
 * Retira caracteres especiais de um campo
 */
function clearField(string $field): string
{
    return str_replace(['.', '-', '/', '(', ')', '_'], '', $field);
}

/**
 * Atribui um erro e seus identificadores à uma sessão
 */
function redirectWithErrors(string $field, string $message, ?string $oldValue, string $redirect)
{
    $_SESSION['error'] = [$field => $message];
    $_SESSION['input-value'] = [$field => $oldValue];
    return header('Location: ' . home_url($redirect));
}

/**
 * Formata string para cpf
 */
function formatCpf(string $cpf): string
{
    return
        substr($cpf, 0, 3) . '.' .
        substr($cpf, 3, 3) . '.' .
        substr($cpf, 6, 3) . '-' .
        substr($cpf, 9, 2);
}

/**
 * Dispara antes de atualizar/registrar um usuário
 */
function mutatorUser(string $userId): void{
    update_user_meta($userId, 'cpf', clearField(get_user_meta($userId)['cpf'][0]));
}
add_action('profile_update', 'mutatorUser', 10, 2);
add_action('user_register', 'mutatorUser', 10, 1);


// Propicia Fazer o ordenamento da query por taxonomia
function orderby_tax_clauses( $clauses, $wp_query ) {
    global $wpdb;
    $taxonomies = get_taxonomies();
    foreach ($taxonomies as $taxonomy) {
        if ( isset( $wp_query->query['orderby'] ) && $taxonomy == $wp_query->query['orderby'] ) {
            if (strpos($clauses['join'], $wpdb->term_relationships) === false) {
                $clauses['join'] .=<<<SQL
    LEFT OUTER JOIN {$wpdb->term_relationships} ON {$wpdb->posts}.ID={$wpdb->term_relationships}.object_id
SQL;
            }
            $clauses['join'] .=<<<SQL
    LEFT OUTER JOIN {$wpdb->term_taxonomy} USING (term_taxonomy_id)
    LEFT OUTER JOIN {$wpdb->terms} USING (term_id)
SQL;
            // $clauses['where'] .= " AND (taxonomy = '{$taxonomy}' OR taxonomy IS NULL)";
            $clauses['groupby'] = "object_id";
            $clauses['orderby'] = "GROUP_CONCAT({$wpdb->terms}.name ORDER BY name ASC) ";
            $clauses['orderby'] .= ( 'ASC' == strtoupper( $wp_query->get('order') ) ) ? 'ASC' : 'DESC';
        }
    }
    return $clauses;
}

add_filter('posts_clauses', 'orderby_tax_clauses', 10, 2 );


//Pesquesa por titulo, content, taxonomia ou meta_value do post
function posts_like_where($where, $wp_query) {
    global $wpdb;
    if ( $post_like = prepare_string($wp_query->get('post_like'))) {
        $where .= ' AND ('.$wpdb->posts.'.post_title LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\'';
        $where .= ' OR '.$wpdb->posts.'.post_content LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\'';
        $where .= ' OR '.$wpdb->postmeta.'.meta_value LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\'';
        $where .= ' OR '.$wpdb->terms.'.name LIKE \'%'.esc_sql($wpdb->esc_like($post_like)).'%\')';
        $where .= ' GROUP BY '.$wpdb->posts.'.ID ';
    }
    return $where;
}
add_filter( 'posts_where', 'posts_like_where', 10, 2 );

function join_tables($join, $wp_query) {
    global $wpdb;
    if($wp_query->get('posts_join') && empty($wp_query->get('tax_query')[0]) && empty($wp_query->get('category_name'))){
        $join .=' JOIN '.$wpdb->postmeta.' ON '.$wpdb->posts.'.ID = '.$wpdb->postmeta.'.post_id ';
        $join .=' JOIN '.$wpdb->term_relationships.' ON '.$wpdb->posts.'.ID = '.$wpdb->term_relationships.'.object_id';
        $join .=' JOIN '.$wpdb->term_taxonomy.' ON '.$wpdb->term_relationships.'.term_taxonomy_id = '.$wpdb->term_taxonomy.'.term_taxonomy_id ';
        $join .=' JOIN '.$wpdb->terms.' ON '.$wpdb->term_taxonomy.'.term_id = '.$wpdb->terms.'.term_id ';
    }
    return $join;
}
add_filter('posts_join', 'join_tables', 10, 2 );


function prepare_string($str){
    $str = strtolower($str);
    $str = str_replace(".", "--", $str);
    $str = str_replace(",", "--", $str);
    $str = str_replace(";", "--", $str);
    $str = str_replace("\"", "--", $str);
    $str = str_replace("select", "--", $str);
    $str = str_replace("drop", "--", $str);
    $str = str_replace("update", "--", $str);
    $str = str_replace("show", "--", $str);

    return preg_replace(array("/(á|à|ã|â|ä)/","/(é|è|ê|ë)/","/(í|ì|î|ï)/","/(ó|ò|õ|ô|ö)/","/(ú|ù|û|ü)/","/(ç)/"),explode(" ","a e i o u c"),$str);
}


// define the wp_mail_failed callback
function action_wp_mail_failed($wp_error){
    return error_log(print_r($wp_error, true));
}
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);





function get_forms_posvenda(){
    global $wpdb;
    $sql = 'SELECT db7.form_value,
        db7.form_date,
        SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(db7.form_value,\'"numero_chamado";\',-1),\'";\',1),\':"\',-1) id
        FROM al_db7_forms db7
        WHERE db7.form_post_id = 297
        AND db7.form_id > 121
        GROUP BY id
    ORDER BY db7.form_date ASC';
    $result = $wpdb->get_results($sql);

    $pos_venda = [['Numero do Chamado', 'Empreendimento', 'Apartamento', 'Nome', 'CPF', 'Telefone', 'Email', 'Identificação (proprietário, inquilino, etc.)', 'Solicitação', 'Data']];

    foreach ($result as $key => $form) {
        $data = (object) unserialize($form->form_value);
        array_push( $pos_venda, [
            $data->numero_chamado ?? '-',
            $data->empreendimento ?? '-',
            $data->apartamento ?? '-',
            $data->nome ?? '-',
            $data->cpf ?? '-',
            $data->telefone ?? '-',
            $data->email ?? '-',
            $data->identificacao ?? '-',
            $data->solicitacao ?? '-',
            $form->form_date ?? '-'
        ]);
    }

    return $pos_venda;
}


function envia_lista(){
    // ini_set('display_errors', 1);
    // ini_set('display_startup_errors', 1);
    // error_reporting(E_ALL);

    require_once 'scripts/google-api-php-client-2.5.0/vendor/autoload.php';

    // $credentials = json_decode(file_get_contents(__DIR__.'/credentials.json'));
    // echo '<pre>';
    // var_dump($credentials);
    // echo '</pre>';

    $forms_posvenda = get_forms_posvenda();

    $spreadsheetId = "1O8HpMpIRHjIYXV0zS7gkJxwxu0COKkNp5rWCTfc3S0M";
    $options = ['valueInputOption' => 'RAW'];

    // PARA MODIFICAR TODAS AS LINHAS DA TABELA
    // $range = 'A1:J'.count($forms_posvenda);
    // $values = $forms_posvenda;

    // AJUSTE PARA MODIFICAR SOMENTE A ULTIMA LINHA DA PLANILHA
    $range = 'A'.count($forms_posvenda).':J'.count($forms_posvenda);
    $values = array(end($forms_posvenda));
	for($i=0; $i<count($values);$i++){
		if(is_array($values[$i][1])){
			$values[$i][1] = $values[$i][1][0];
		}
	}
    // caso seja necessario importar mais do que a ultima linha
    // $range = 'A'.(count($forms_posvenda)-4).':J'.count($forms_posvenda);
    // $values = array_slice($forms_posvenda, +1, 5, false);
    $client = new \Google_Client();
    $client->setApplicationName('PosVendaMelchioretto');
    $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
    $client->setAccessType('offline');
    $client->setAuthConfig(__DIR__.'/credentials.json');
    $service = new Google_Service_Sheets($client);

    $body   = new Google_Service_Sheets_ValueRange(['values' => $values]);
    $result = $service->spreadsheets_values->update($spreadsheetId, $range, $body, $options);
    // print($result->updatedRange. PHP_EOL);

}


add_action("wpcf7_before_send_mail", "atualiza_lista_pos_venda");
function atualiza_lista_pos_venda($cf7) {
    envia_lista();
    $wpcf = WPCF7_ContactForm::get_current();
    return $wpcf;
}



function atualiza_chamados() {
    register_rest_route( 'envia', '/lista', array(
        'methods'  => WP_REST_Server::READABLE,
        'callback' => 'envia_lista',
    ) );
}

add_action( 'rest_api_init', 'atualiza_chamados' );
