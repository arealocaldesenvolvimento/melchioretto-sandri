<!DOCTYPE html>
<html <?php language_attributes() ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5QQNDZT');</script>
    <!-- End Google Tag Manager -->

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TBR2955');</script>
<!-- End Google Tag Manager -->
	<meta name="google-site-verification" content="dJv3GximS4OkK43xIqaZec5enUmvnPq3r3GzbQs5mW4" />
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#00274d">
    <link rel="icon" href="<?= get_image_url('favicon.png') ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css"
        integrity="sha256-zmfNZmXoNWBMemUOo1XUGFfc0ihGGLYdgtJS3KCr/l0="
        crossorigin="anonymous"
    />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url') ?>">
    <title><?php wp_title('') ?></title>
    <?php wp_head() ?>
</head>
<body <?php body_class() ?>>

    <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QQNDZT"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TBR2955"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <header id="desktop-menu">
        <nav class="fixed-top-menu">
            <div class="container">
                <?php wp_nav_menu(array('menu'=> 'fixed-top', 'theme_location' => 'principal', 'container' => false)); ?>
            </div>
        </nav>
        <nav class="main-menu">
            <div class="container">
                <div class="left-right-content">
                    <div class="left">
                        <?php get_logo('logo-branco.png'); ?>
                    </div>
					<?php wp_nav_menu(array('menu'=> 'principal', 'theme_location' => 'principal', 'container' => false)); ?>
					<form class="cidade">
						<?php
							$terms = get_terms('cidade', array(
								'hide_empty' =>  false,
							));
						?>

						<select>
							<option disabled selected>Onde você deseja morar?</option>
							<?php
								foreach ($terms as $value) {
									echo "<option value='".get_category_link($value->term_id)."#empreendimentos'>". $value->name."</option>";
								}
							?>
						</select>
					</form>
                </div>
            </div>
        </nav>
    </header>

    <div id="wrapper">
