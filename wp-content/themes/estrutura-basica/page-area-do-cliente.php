<?php

session_start();

if (!empty($_POST['cpf'])) {
    global $wpdb;

    $cpf = clearField($_POST['cpf']);

    if (strlen($cpf) < 11) {
        redirectWithErrors('cpf', 'Por favor, informe um CPF válido', $cpf, '/central-do-cliente');
    }

    $user = $wpdb->get_results("
        SELECT al_users.ID, al_users.user_nicename
        FROM al_users
        LEFT JOIN al_usermeta
        ON al_users.ID = al_usermeta.user_id
        WHERE al_usermeta.meta_key = 'cpf' AND al_usermeta.meta_value = $cpf;
    ")[0];

    $userId = $user->ID;
    if (empty($userId)) {
        redirectWithErrors('cpf', 'Por favor, informe um CPF válido', $cpf, '/central-do-cliente');
    }

    $empreedimentos = get_field('empreendimentos_usuario', "user_$userId");
} else {
    redirectWithErrors('cpf', 'Por favor, informe um CPF válido', $cpf, '/central-do-cliente');
}

get_header();
the_content();

?>
    <section class="area-do-cliente">
        <div class="small-container">
            <div class="title animated fadeInDown">
                <h2>Olá <?= $user->user_nicename ?></h2>
                <a href="<?= get_home_url('/') ?>" title="Melchioretto Sandri">
                    <?php get_logo('logo-branco.png') ?>
                </a>
            </div>
            <?php if (!empty($empreedimentos)): ?>
                <section class="empreendimentos animated fadeInUp">
                    <?php foreach ($empreedimentos as $imovel): ?>
                        <?php
                            $id = $imovel->ID;
                            $title = $imovel->post_title;
                        ?>
                        <div class="empreendimento-card">
                            <img
                                src="<?= get_field('imagem_destacada', $id)['sizes']['large'] ?>"
                                alt="<?= $title ?>"
                                title="<?= $title ?>"
                            >
                            <div class="text-content">
                                <a
                                    href="<?= get_permalink($id) ?>"
                                    alt="<?= $title ?>"
                                    title="<?= $title ?>"
                                    target="_blank"
                                >
                                    <h2><?= $title ?></h2>
                                </a>
                                <div class="files">
                                    <?php if ($arquivos = get_field('arquivos', $id)): ?>
                                        <?php foreach ($arquivos as $arquivo): ?>
                                            <?php $filename = $arquivo['filename'] ?>
                                            <a
                                                download
                                                href="<?= $arquivo['url'] ?>"
                                                title="<?= $filename ?>"
                                            >
                                                <img
                                                    src="<?= get_image_url('icones/'.$arquivo['subtype'].'.svg') ?>"
                                                    alt="<?= $filename ?>"
                                                    title="<?= $filename ?>"
                                                >
                                                <span>
                                                    <?=
                                                        strlen($filename) > 14
                                                            ? substr($filename, 0, 14).'...'
                                                            : $filename
                                                    ?>
                                                </span>
                                            </a>
                                        <?php endforeach ?>
                                    <?php else: ?>
                                        <p>Nenhum arquivo cadastrado para este empreendimento</p>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </section>
            <?php else: ?>
                <div class="notfound animated fadeInUp">
                    <h2>Desculpe, mas nenhum empreendimento está associado a este CPF</h2>
                    <a href="<?= home_url('/central-do-cliente') ?>" title="Voltar">
                        Voltar
                    </a>
                </div>
            <?php endif ?>
        </div>
    </section>

<?php get_footer() ?>
