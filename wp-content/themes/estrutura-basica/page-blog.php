<?php get_header() ?>
	<?php if(have_rows("banner_topo")){ ?>
		<?php while(have_rows("banner_topo")){ the_row() ?>
			<section class="banner_topo" style="background-image: url(<?= get_sub_field("imagem")['url'] ?>);">
				<div class="container">
					<div class="texto">
						<?= get_sub_field("texto") ?>
					</div>
				</div>
			</section>
		<?php } ?>
	<?php }else{ ?>
		<section class="banner_topo" style="background-image: url(<?= get_image_url("fundo_neutro.png") ?>);">
			<div class="container">
				<div class="texto">
					<h1><?= get_the_title() ?></h1>
				</div>
			</div>
		</section>
	<?php } ?>
    <?php
        $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
        $pesquisa = $_GET['pesquisa'] ?? '';
        $posts = new WP_Query([
            'post_type' => 'post',
            'posts_per_page' => 4,
            'posts_join' => !empty($_GET['pesquisa']) ? true : false,
            'post_like' => $pesquisa,
            'orderby' => 'date',
            'paged'  => $paged,
        ])
    ?>
    <section class="small-container blog" id="blog">
        <form id="busca" class="busca" action="#blog" method="get">
            <input type="text" name="pesquisa" placeholder="Buscar" value="<?= $_GET['pesquisa'] ?? '' ?>"/>
            <a href="javascript:{}" onclick="document.getElementById('busca').submit();" class="submit"><i class="fa fa-search" aria-hidden="true"></i></a>
            </span>
        </form>
        <div>
            <?php
            if (!$posts->have_posts())
                echo '<p class="no-result" style="text-align: center;">Nenhum Resultado Correspondente Encontrado</p>';

            $count = 1;
            while ($posts->have_posts()): $posts->the_post();
                    $id = get_the_ID();
                    $title = get_the_title();
                    $position = $count % 2 === 0 ? 'even' : 'odd';
                ?>
                <article class="<?= $position ?>">
                    <a
                        class="<?= $position ?>"
                        href="<?= get_permalink($id) ?>"
                        alt="<?= $title ?>"
                        title="<?= $title ?>"
                    >
                        <img
                            src="<?= get_the_post_thumbnail_url($id, 'large') ?>"
                            alt="<?= $title ?>"
                            title="<?= $title ?>"
                        >
                    </a>
                    <div class="text-content <?= $position ?>">
                        <a
                            href="<?= get_permalink($id) ?>"
                            alt="<?= $title ?>"
                            title="<?= $title ?>"
                        >
                            <h2><?= $title ?></h2>
                        </a>
                        <p><?= get_the_excerpt() ?></p>
                    </div>
                </article>
            <?php $count ++; endwhile ?>
        </div>
        <div class="mais">
            <?= get_next_posts_link('Carregar mais', $posts->max_num_pages) ?>
        </div>
    </section>

<?php get_footer() ?>
