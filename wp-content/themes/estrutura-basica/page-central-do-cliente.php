<?php session_start(); get_header(); the_content() ?>

    <section class="central-do-cliente">
        <div class="small-container">
            <div class="title animated fadeInRight">
                <h2>Central do Cliente</h2>
                <a href="<?= get_home_url('/') ?>" title="Melchioretto Sandri">
                    <?php get_logo('logo-branco.png') ?>
                </a>
            </div>
            <article class="animated fadeInLeft">
                <h1>Mais agilidade e facilidade para você</h1>
                <p>Bem-vindo ao Novo Portal do Cliente!</p>
                <p>Para o Primeiro Acesso clique aqui.</p>
                <p>Dúvidas entre em contato com o SAC (47) 3520-2300</p>
                <h2>Entre com os dados de acesso abaixo:</h2>
                <form action="<?= home_url('/area-do-cliente') ?>" method="POST" role="form">
                    <input
                        type="text"
                        name="cpf"
                        id="cpf"
                        class="<?= $_SESSION['error']['cpf'] ? 'input-error' : '' ?>"
                        value="
                            <?=
                                !empty($_SESSION['input-value']['cpf'])
                                    ? formatCpf($_SESSION['input-value']['cpf'])
                                    : ''
                            ?>
                        "
                    >
                    <?php if (!empty($_SESSION['error'])): ?>
                        <h4 class="text-error"><?= $_SESSION['error']['cpf'] ?></h4>
                    <?php endif ?>
                    <input type="submit" name="submit" value="Entrar!">
                </form>
            </article>
        </div>
    </section>

<?php get_footer(); session_destroy() ?>
