<?php get_header(); ?>
	<?php if(have_rows("banner_topo")){ ?>
		<?php while(have_rows("banner_topo")){ the_row() ?>
			<section class="banner_topo" style="background-image: url(<?= get_sub_field("imagem")['url'] ?>);">
				<div class="container">
					<div class="texto">
						<?= get_sub_field("texto") ?>
					</div>
				</div>
			</section>
		<?php } ?>
	<?php }else{ ?>
		<section class="banner_topo" style="background-image: url(<?= get_image_url("fundo_neutro.png") ?>);">
			<div class="container">
				<div class="texto">
					<h1><?= get_the_title() ?></h1>
				</div>
			</div>
		</section>
	<?php } ?>

    <article id="id-<?php the_ID() ?>" <?php post_class() ?>>
        <?php
            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
            $empreendimentos = new WP_Query([
                'post_type' => 'empreendimento',
                'posts_per_page' => 9,
                'orderby' => 'status',
                'order' => 'DESC',
                'tax_query' => [
                    term_exists(get_queried_object_id(), 'cidade') ? [
                        'taxonomy' => 'cidade',
                        'field' => 'term_id',
                        'terms' => [get_queried_object_id()],
                    ] : '',
                    term_exists(get_queried_object_id(), 'status') ? [
                        'taxonomy' => 'status',
                        'field' => 'term_id',
                        'terms' => [get_queried_object_id()],
                    ] : '',
                    (term_exists(get_queried_object_id(), 'status')
                    || term_exists(get_queried_object_id(), 'cidade')) ? '' : [
                        'taxonomy' => 'status',
                        'field'    => 'slug',
                        'terms'    => array('lancamento','em-obras'),
                        'operator' => 'IN',
                   ],
                    (term_exists(get_queried_object_id(), 'status')
                    || term_exists(get_queried_object_id(), 'cidade')) ? '' : [
                        'taxonomy' => 'status',
                        'field'    => 'slug',
                        'terms'    => array('obra-entregue','pronto-para-morar'),
                        'operator' => 'NOT IN',
                   ],
                ],
                'paged'  => $paged
            ]);
        ?>
        <div class="entry-content">
            <?php
                global $post, $posts, $found_posts, $post_count;
                $old = $post;
                $post = get_post(get_page_by_path('empreendimentos')->ID);
                the_content();
                $post = $old;
            ?>
            <section class="empreendimentos container" id="empreendimentos">
                <h1 class="title">Conheça nossos<br/>empreendimentos</h1>
                <div class="left">
                    <div class="menu">
                        <?php
                            $terms = get_terms('cidade', [
                                'hide_empty' =>  false,
                            ]);
                        ?>
                        <p>Em Qual Local?</p>
                        Cidade
                        <select>
                            <option disabled selected>Selecione</option>
                            <?php
                            foreach ($terms as $value) {
                                echo "<option value='".get_category_link($value->term_id)."#empreendimentos'>". $value->name."</option>";
                            }
                            ?>
                        </select>
                        <?php
                            $terms = get_terms('status', [
                                'hide_empty' =>  false,
                            ]);
                        ?>
                        <p>Fase da obra</p>
                        <ul>
                            <?php
                                foreach ($terms as $value) {
                                    echo "<li><a href='".get_category_link($value->term_id)."#empreendimentos'>". $value->name."</a></li>";
                                }
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="right">
                    <?php
                    if (!$empreendimentos->have_posts())
                    echo '<p class="no-result" style="text-align: center;">Nenhum Resultado Correspondente Encontrado</p>';

                    while ($empreendimentos->have_posts()): $empreendimentos->the_post() ?>
                        <div class='article-container'>
                            <article class="archive <?= get_the_terms(get_the_ID(), 'status')[0]->slug ?> <?= get_field('conheca_mais')['unidades_disponiveis'] ?>">
                                <div class='imagem'>
                                    <a href="<?= get_permalink() ?>">
                                        <img src='<?= get_field('imagem_destacada', get_the_ID())['url'] ?>'>
                                        <strong class="<?= get_the_terms(get_the_ID(), 'status')[0]->slug ?>">
                                            <?= get_the_terms(get_the_ID(), 'status')[0]->name ?>
                                        </strong>
                                    </a>
                                </div>
                                <div class='content'>
                                    <div class='cidade'><p><?= get_the_terms(get_the_ID(), 'cidade')[0]->name ?></p></div>
                                    <div class='titulo'>
                                        <a href="<?= get_permalink() ?>">
                                            <h3><?= get_the_title() ?></h3>
                                        </a>
                                    </div>

                                    <?php
                                    $anterior = '';
                                    $count = 1;
                                    if(have_rows('plantas')): ?>
                                        <div class='area'>
                                            <div class='left'>
                                                <i class="fas fa-ruler-combined"></i>
                                            </div>
                                            <div class='right'>
                                                <?php while (have_rows('plantas')): the_row() ?>
                                                    <?php if (!empty(get_sub_field('area_total')) && get_sub_field('area_total') <=> $anterior): ?>
                                                        <!-- <p>
                                                            <?= get_sub_field('mais_informacoes')[0]['informacao'] ?>
                                                            <strong>Área: <?= get_sub_field('area_total') ?> m²</strong>
                                                        </p> -->
                                                        <?php
                                                            // $anterior = get_sub_field('area_total');
                                                            // $count++;
                                                            if(empty($anterior) || $anterior < get_sub_field('area_total')){
                                                                $anterior = get_sub_field('area_total');
                                                            }
                                                        ?>
                                                    <?php endif ?>
                                                <?php endwhile ?>
                                                <p>
                                                    <strong>Área Total de até: <?= $anterior ?> m²</strong>
                                                </p>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <?php if (!empty(get_field('conheca_mais')['total_de_aptos']) && $count < 5): ?>
                                        <div class='area'>
                                            <div class='left'>
                                                <i class="far fa-building"></i>
                                            </div>
                                            <div class='right'>
                                                <p>Total de aptos: <?= get_field('conheca_mais')['total_de_aptos'] ?></p>
                                            </div>
                                        </div>
                                    <?php $count++; endif ?>

                                    <?php if (!empty(get_field('conheca_mais')['quantidade_de_blocos']) && $count < 5): ?>
                                        <div class='area'>
                                            <div class='left'>
                                                <i class="fas fa-th-large"></i>
                                            </div>
                                            <div class='right'>
                                                <p>
                                                	<?= get_field('conheca_mais')['quantidade_de_blocos'] ?>
                                                	<?= (get_field('conheca_mais')['quantidade_de_blocos'] > 1) ? "blocos" : "bloco" ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php $count++; endif ?>

                                    <?php if (!empty(get_field('conheca_mais')['quantidade_saloes_festa'] ) && $count < 5): ?>
                                        <div class='area'>
                                            <div class='left'>
                                                <i class="fas fa-gifts"></i>
                                            </div>
                                            <div class='right'>
                                                <p>
                                                	<?= get_field('conheca_mais')['quantidade_saloes_festa'] ?>
                                                	<?= (get_field('conheca_mais')['quantidade_saloes_festa'] > 1) ? "salões de festas" : "salão de festas" ?>
                                                </p>
                                            </div>
                                        </div>
                                    <?php $count++; endif ?>

                                    <?php if ($count < 5): ?>
                                        <?php if (!empty(get_field('conheca_mais')['vaga_de_estacionamento'][0])): ?>
                                            <div class='area'>
                                                <div class='left'>
                                                    <i class="fas fa-warehouse"></i>
                                                </div>
                                                <div class='right'>
                                                    <p><?= get_field('conheca_mais')['vaga_de_estacionamento'][0] ? 'Vaga de estacionamento' :  '' ?></p>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    <?php $count++; endif ?>
                                </div>
                            </article>
                        </div>
                    <?php endwhile; ?>
                    <?php if(!empty(get_next_posts_link('Carregar mais!', $empreendimentos->max_num_pages))): ?>
                        <div class="mais">
                            <?= get_next_posts_link('Carregar mais!', $empreendimentos->max_num_pages) ?>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </article>

<?php get_footer() ?>
