<?php get_header(); ?>
	<?php if(have_rows("banner_topo")){ ?>
		<?php while(have_rows("banner_topo")){ the_row() ?>
			<section class="banner_topo" style="background-image: url(<?= get_sub_field("imagem")['url'] ?>);">
				<div class="container">
					<div class="texto">
						<?= get_sub_field("texto") ?>
					</div>
				</div>
			</section>
		<?php } ?>
	<?php }else{ ?>
		<section class="banner_topo" style="background-image: url(<?= get_image_url("fundo_neutro.png") ?>);">
			<div class="container">
				<div class="texto">
					<h1><?= get_the_title() ?></h1>
				</div>
			</div>
		</section>
	<?php } ?>
    <?php
        $materiais = new WP_Query([
            'post_type' => 'materiais',
            'posts_per_page' => 6,
            'orderby' => 'date',
        ])
    ?>
    <section class="small-container materiais">
        <?php while ($materiais->have_posts()): $materiais->the_post() ?>
            <?php
                $id = get_the_ID();
                $title = get_the_title()
            ?>
            <article>
                <img
                    src="<?= get_the_post_thumbnail_url($id, 'large') ?>"
                    alt="<?= $title ?>"
                    title="<?= $title ?>"
                >
                <h2><?= $title ?></h2>
                <p><?= get_the_excerpt() ?></p>
                <a
                    href="<?= get_field('link', $id) ?>"
                    title="Baixar Gratuitamente"
                    target="_blank"
                >
                    Baixar Gratuitamente
                </a>
            </article>
        <?php endwhile ?>
    </section>
<?php get_footer() ?>
