<?php get_header() ?>
	<?php if(have_rows("banner_topo")){ ?>
		<?php while(have_rows("banner_topo")){ the_row() ?>
			<section class="banner_topo" style="background-image: url(<?= get_sub_field("imagem")['url'] ?>);">
				<div class="container">
					<div class="texto">
						<?= get_sub_field("texto") ?>
					</div>
				</div>
			</section>
		<?php } ?>
	<?php }else{ ?>
		<section class="banner_topo" style="background-image: url(<?= get_image_url("fundo_neutro.png") ?>);">
			<div class="container">
				<div class="texto">
					<h1><?= get_the_title() ?></h1>
				</div>
			</div>
		</section>
	<?php } ?>
    <article id="id-<?php the_ID() ?>" <?php post_class() ?>>
        <div class="entry-content">
            <?php the_content() ?>
        </div>
    </article>

<?php get_footer() ?>
