/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./assets/js/funcoes/funcoes.js":
/*!**************************************!*\
  !*** ./assets/js/funcoes/funcoes.js ***!
  \**************************************/
/***/ (() => {

$(document).ready(function () {
  if (!document.addEventListener) {
    window.location = 'http://www.arealocal.com.br/ie6/oculta-site-ie6.php';
  }
  var templateUrl = al_url['templateUrl'];
  var homeUrl = al_url['homeUrl'];

  /**
   * Form página "central-do-cliente"
   */
  $('input#cpf').prop('placeholder', 'CPF');
  $('input#user_pass').prop('placeholder', 'Senha');

  /**
   * Carrega mais postagens (page-blog)
   */
  $('.blog .mais a').on('click', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    var text_load = $('.blog .mais a').html();
    $('.blog .mais a').html('Carregando...');
    $.get(link, function (data) {
      var post = $('.blog >div >article ', data);
      $('.blog >div:not(.mais)').append(post);
      if ($('.blog .mais a', data).attr('href')) {
        $('.blog .mais a').attr('href', $('.blog .mais a', data).attr('href'));
        $('.blog .mais a').html(text_load);
      } else {
        $('.blog .mais').remove();
      }
    });
  });

  /**
   * Carrega mais postagens (page-blog)
   */
  $('#empreendimentos .mais a').on('click', function (event) {
    event.preventDefault();
    var link = $(this).attr('href');
    var text_load = $('#empreendimentos .mais a').html();
    $('#empreendimentos .mais a').html('Carregando...');
    $.get(link, function (data) {
      var post = $('#empreendimentos >.right >.article-container', data);
      $('#empreendimentos >.right').append(post);
      if ($('#empreendimentos .mais a', data).attr('href')) {
        $('#empreendimentos .mais a').attr('href', $('#empreendimentos .mais a', data).attr('href'));
        $('#empreendimentos .mais a').html(text_load);
      } else {
        $('#empreendimentos .mais').remove();
      }
    });
  });

  /**
   * Inputs formulários
   */
  $('input#telefone').mask('(00) 0000-00009');
  $('input#cpf, input[name="cpf"]').mask('000.000.000-00', {
    reverse: true
  });

  /**
   * Número chamado (formulário page pós-venda)
   */
  $('div#wpcf7-f297-p296-o1 form').ready(function () {
    $('input#numero_chamado').val(Date.now());
  });

  // SLIDER EMPREENDIMENTO HOME
  $('#sliderCard').lightSlider({
    item: 3,
    gallery: true,
    pager: false,
    slideMargin: 50,
    onSliderLoad: function onSliderLoad() {
      $('#sliderCard').removeClass('cS-hidden');
    },
    responsive: [{
      breakpoint: 800,
      settings: {
        item: 1,
        slideMove: 1,
        slideMargin: 6
      }
    }]
  });

  // SLIDER EMPREENDIMENTO HOME
  $('#ultimas_do_blog').lightSlider({
    item: 1,
    auto: true,
    speed: 1200,
    pause: 8000,
    controls: false,
    slideMargin: 50,
    onSliderLoad: function onSliderLoad() {
      $('#ultimas_do_blog').removeClass('cS-hidden');
    }
  });

  //CONTADOR DINAMICO NA HOME
  if (window.matchMedia('screen and (min-width: 960px)').matches) {
    $(document).scroll(function () {
      if ($(window).scrollTop() > 1000) {
        counter();
      }
    });
  } else {
    counter();
  }

  /** BUSCA HEADER POR CIDADE */
  $('.main-menu form.cidade').submit(function (event) {
    event.preventDefault();
    var valor = $('form.cidade select').val();
    valor ? document.location.href = valor : document.location.href = (document.origin === undefined ? 'https://melchiorettosandri.com.br' : document.origin) + '/empreendimentos';
  });

  /** BUSCA MENU EMPREENDIMENTOS POR CIDADE */
  $('.menu > select').change(function () {
    var valor = $(this).val();
    valor ? document.location.href = valor : document.location.href = document.origin + '/empreendimentos';
  });

  /**
   * Página single empreendimento
   */
  $('body.single-empreendimento').ready(function () {
    /**
     * Barras de progresso (single empreendimento)
     */
    $('.status div[id^="circle-"]').each(function () {
      var classe = $(this).find('input').attr('class');
      $('#circle-' + classe).circleProgress({
        value: $('.' + classe).val() === '100' ? 1 : Number($('.' + classe).val()) / 100,
        size: 120,
        fill: {
          color: '#308300'
        },
        animation: {
          duration: 2200,
          easing: 'circleProgressEasing'
        }
      });
    });

    /**
     * Light slider single empreendimento 01
     */
    $('#single-empreendimento-slider-01').lightSlider({
      gallery: true,
      item: 1,
      loop: false,
      thumbItem: 5,
      slideMargin: 0,
      enableDrag: true,
      currentPagerPosition: 'left',
      thumbMargin: 20,
      thumbnail: true,
      enableThumbDrag: true,
      enableThumbSwipe: true,
      responsive: [{
        breakpoint: 480,
        settings: {
          thumbItem: 2
        }
      }],
      onSliderLoad: function onSliderLoad(el) {
        el.lightGallery({});
      }
    });

    /**
     * Light slider plantas
     */
    $('#single-empreendimento-slider-plantas').lightSlider({
      item: 1,
      loop: false,
      slideMargin: 0,
      keyPress: true,
      pager: false,
      controls: true,
      speed: 500,
      auto: false,
      currentPagerPosition: 'left',
      responsive: [{
        breakpoint: 480,
        settings: {
          slideMargin: 30
        }
      }, {
        breakpoint: 375,
        settings: {
          slideMargin: 60
        }
      }],
      onSliderLoad: function onSliderLoad() {
        $('#single-empreendimento-slider-plantas').removeClass('cS-hidden');
      }
    });

    //RENDER GOOGLE MAPS API
    var latitude = document.querySelector('.lat').value ? document.querySelector('.lat').value : null;
    var longitude = document.querySelector('.lng').value ? document.querySelector('.lng').value : null;
    maps(latitude, longitude);
    function maps(lat, lng) {
      var templateUrl = al_url['templateUrl'];
      var marker_url = templateUrl + '/assets/images/cd-icon-location.png',
        map_zoom = 14;
      var main_color = '#2d313f',
        saturation_value = -20,
        brightness_value = 5;

      //set google map options
      var map_options = {
        center: new google.maps.LatLng(lat, lng),
        zoom: map_zoom,
        panControl: false,
        zoomControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
        // styles: style,
      };
      //inizialize the map
      var map = new google.maps.Map(document.querySelector('#google-container'), map_options);
      //add a custom marker to the map
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        map: map,
        visible: true,
        icon: marker_url
      });

      //add custom buttons for the zoom-in/zoom-out on the map
      function CustomZoomControl(controlDiv, map) {
        //grap the zoom elements from the DOM and insert them in the map
        var controlUIzoomIn = document.getElementById('cd-zoom-in'),
          controlUIzoomOut = document.getElementById('cd-zoom-out');
        controlDiv.appendChild(controlUIzoomIn);
        controlDiv.appendChild(controlUIzoomOut);

        // Setup the click event listeners and zoom-in or out according to the clicked element
        google.maps.event.addDomListener(controlUIzoomIn, 'click', function () {
          map.setZoom(map.getZoom() + 1);
        });
        google.maps.event.addDomListener(controlUIzoomOut, 'click', function () {
          map.setZoom(map.getZoom() - 1);
        });
      }
      var zoomControlDiv = document.createElement('div');
      var zoomControl = new CustomZoomControl(zoomControlDiv, map);

      //insert the zoom div on the top left of the map
      map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
    }
  });
});
function counter() {
  $('.count').each(function () {
    var $this = $(this),
      countTo = $this.attr('data-count');
    $({
      countNum: $this.text()
    }).animate({
      countNum: countTo
    }, {
      duration: 1500,
      easing: 'linear',
      step: function step() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function complete() {
        $this.text(this.countNum);
      }
    });
  });
}

/***/ }),

/***/ "./assets/css/scss/estilo.scss":
/*!*************************************!*\
  !*** ./assets/css/scss/estilo.scss ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/public/js/funcoes": 0,
/******/ 			"style": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkestrutura_basica"] = self["webpackChunkestrutura_basica"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["style"], () => (__webpack_require__("./assets/js/funcoes/funcoes.js")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["style"], () => (__webpack_require__("./assets/css/scss/estilo.scss")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;