<?php

flush_rewrite_rules();

/**
 * Empreendimento
 */
function type_post_empreendimento()
{
    $labels = [
        'name' => _x('Empreendimento', 'post type general name'),
        'singular_name' => _x('Empreendimento', 'post type singular name'),
        'add_new' => _x('Adicionar Novo', 'Novo item'),
        'add_new_item' => __('Novo Empreendimento'),
        'edit_item' => __('Editar Empreendimento'),
        'new_item' => __('Novo Empreendimento'),
        'view_item' => __('Ver Empreendimento'),
        'search_items' => __('Procurar Empreendimentos'),
        'not_found' =>  __('Nenhuma empreendimento encontrado'),
        'not_found_in_trash' => __('Nenhum empreendimento encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Empreendimentos'
    ];

    $args = [
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 5,
        'menu_icon' => 'dashicons-admin-home',
        'supports' => ['title', 'editor', 'thumbnail', 'custom-fields', 'revisions']
    ];

    register_post_type('empreendimento' , $args);

    register_taxonomy(
        'cidade',
        'empreendimento',
        [
            'label' => 'Cidade',
            'singular_label' => 'Cidade',
            'rewrite' => true,
            'hierarchical' => true
        ]
    );

    register_taxonomy(
        'status',
        'empreendimento',
        [
            'label' => 'Status da Obra',
            'singular_label' => 'Status',
            'rewrite' => true,
            'hierarchical' => true
        ]
    );

}
add_action('init', 'type_post_empreendimento');

/**
 * Materiais
 */
function materiais()
{
    register_post_type('materiais', [
        'labels' => [
            'name' => _x('Materiais', 'post type general name'),
            'singular_name' => _x('Material', 'post type singular name'),
            'add_new' => __('Adicionar novo material'),
            'add_new_item' => __('Adicionar novo material'),
            'edit_item' => __('Editar material'),
            'new_item' => __('Novo material'),
            'view_item' => __('Ver material'),
            'not_found' =>  __('Materiais não encontrados'),
            'not_found_in_trash' => __('Nenhum material foi encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Materiais'
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-media-document',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'editor', 'custom-fields', 'thumbnail']
    ]);
}
add_action('init', 'materiais');
