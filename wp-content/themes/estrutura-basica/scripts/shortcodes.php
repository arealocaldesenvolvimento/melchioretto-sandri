<?php

/**
 * Página home
 */
function empreendimentos_shortcode() {
    $empreendimentos = new WP_Query([
        'post_type' => 'empreendimento',
        'posts_per_page' => 3,
        'tax_query' => array(
           array(
            'taxonomy' => 'status',
            'field'    => 'slug',
            'terms'    => array('lancamento','em-obras'),
            'operator' => 'IN',
           )
        ),
        'orderby' => array('status' => 'DESC', 'date' => 'DESC')
    ]);
    ?>
    <div class="empreendimentos-card">
        <h1>Conheça nossos<br>empreendimentos</h1>
        <ul id="sliderCard" class="cS-hidden">
            <?php while ($empreendimentos->have_posts()): $empreendimentos->the_post() ?>
                <li>
                    <div class='article-container'>
                        <article class="<?= get_the_terms(get_the_ID(), 'status')[0]->slug ?>">
                            <div class='imagem'>
                                <a href="<?= get_permalink() ?>">
                                    <img src='<?= get_field('imagem_destacada', get_the_ID())['url'] ?>'>
                                    <strong class="<?= get_the_terms(get_the_ID(), 'status')[0]->slug ?>">
                                        <?= get_the_terms(get_the_ID(), 'status')[0]->name ?>
                                    </strong>
                                </a>
                            </div>
                            <div class='content'>
                                <div class='cidade'><p><?= get_the_terms(get_the_ID(), 'cidade')[0]->name ?></p></div>
                                <div class='titulo'>
                                    <a href="<?= get_permalink() ?>">
                                        <h3><?= get_the_title() ?></h3>
                                    </a>
                                </div>

                                <?php
                                $anterior = '';
                                $count = 1;
                                if(have_rows('plantas')): ?>
                                    <div class='area'>
                                        <div class='left'>
                                            <i class="fas fa-ruler-combined"></i>
                                        </div>
                                        <div class='right'>
                                            <?php while (have_rows('plantas')): the_row() ?>
                                                <?php if (!empty(get_sub_field('area_total')) && get_sub_field('area_total') <=> $anterior): ?>
                                                    <?= get_sub_field('mais_informacoes')[0]['informacao'] ?>
                                                    <?php
                                                        if(empty($anterior) || $anterior < get_sub_field('area_total')){
                                                            $anterior = get_sub_field('area_total');
                                                        }
                                                    ?>
                                                <?php endif ?>
                                            <?php endwhile ?>
                                            <p>
                                                <strong>Área Total de até: <?= $anterior ?> m²</strong>
                                            </p>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <?php if (!empty(get_field('conheca_mais')['total_de_aptos']) && $count < 5): ?>
                                    <div class='area'>
                                        <div class='left'>
                                            <i class="far fa-building"></i>
                                        </div>
                                        <div class='right'>
                                            <p>Total de aptos: <?= get_field('conheca_mais')['total_de_aptos'] ?></p>
                                        </div>
                                    </div>
                                <?php $count++; endif ?>

                                <?php if (!empty(get_field('conheca_mais')['quantidade_de_blocos']) && $count < 5): ?>
                                    <div class='area'>
                                        <div class='left'>
                                            <i class="fas fa-th-large"></i>
                                        </div>
                                        <div class='right'>
                                            <p><?= get_field('conheca_mais')['quantidade_de_blocos'] ?> blocos</p>
                                        </div>
                                    </div>
                                <?php $count++; endif ?>

                                <?php if (!empty(get_field('conheca_mais')['quantidade_saloes_festa'] ) && $count < 5): ?>
                                    <div class='area'>
                                        <div class='left'>
                                            <i class="fas fa-gifts"></i>
                                        </div>
                                        <div class='right'>
                                            <p>
                                                <?= get_field('conheca_mais')['quantidade_saloes_festa'] ?>
                                                <?= get_field('conheca_mais')['quantidade_saloes_festa'] !== '1' ? 'salões de festa' : 'salão de festa' ?>
                                            </p>
                                        </div>
                                    </div>
                                <?php $count++; endif ?>

                                <?php if ($count < 5): ?>
                                    <div class='area'>
                                        <?php if (!empty(get_field('conheca_mais')['vaga_de_estacionamento'][0])): ?>
                                            <div class='left'>
                                                <i class="fas fa-warehouse"></i>
                                            </div>
                                            <div class='right'>
                                                <p>Vaga de estacionamento</p>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                <?php $count++; endif ?>
                            </div>
                        </article>
                    </div>
                </li>
            <?php endwhile ?>
        </ul>
    </div>

    <div class="mais">
        <a href="<?= get_site_url() . '/empreendimentos' ?>">Conheça mais!</a>
    </div>
    <?php
}
add_shortcode('empreendimentos', 'empreendimentos_shortcode');

/**
 * Contador home
 */
function counter_shortcode()
{
    ?>
        <div class="counter">
            <div>
                <p class="count" data-count="5000">0</p>
                <p>Apartamentos em</p>
                <p>Planejamento</p>
            </div>

            <div>
                <p class="count" data-count="250">0</p>
                <p>Mil m² em</p>
                <p>Área Construída</p>
            </div>

            <div>
                <p class="count" data-count="3900">0</p>
                <p>Apartamentos</p>
                <p>Entregues</p>
            </div>
        </div>
    <?php
}
add_shortcode('counter', 'counter_shortcode');

/**
 * Página "contato"
 */
function page_contato_shortcode()
{
    ?>
        <section class="contato">
            <div class="small-container">
                <div class="title">
                    <h2>Fale Conosco</h2>
                    <span><i class="fas fa-phone-alt"></i> (47) 3520-2300</span>
                </div>
                <p>Envie suas dúvidas ou sugestões, em breve entraremos em contato</p>
                <?= do_shortcode('[contact-form-7 id="5" title="Contato"]') ?>
                <div class="localizacao">
                    <h2>Localização</h2>
                    <p><i class="fas fa-map-marked-alt fa-2x"></i> Alameda Bela Aliança, 250 - Jardim América, Rio do Sul - SC, 89160-000</p>
                </div>
            </div>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14192.792099483517!2d-49.643197!3d-27.2129319!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xca6b5e5e47e8c2a2!2sGrupo%20Melchioretto%20Sandri!5e0!3m2!1spt-BR!2sbr!4v1576522647620!5m2!1spt-BR!2sbr"
                frameborder="0"
                allowfullscreen="no"
            >
            </iframe>
        </section>
    <?php
}
add_shortcode('page_contato', 'page_contato_shortcode');

/**
 * Página "investidores"
 */
function page_investidores_shortcode($args)
{
    $idPaginaInvestidores = 427;
    ?>
        <div class="investidores-mais">
            <a
                href="<?= $args["link"] ?>"
                title="Confira mais!"
            >
                Confira mais!
            </a>
        </div>
    <?php
}
add_shortcode('page_investidores', 'page_investidores_shortcode');

/**
 * Página "institucional"
 */
function page_institucional_shortcode()
{
    ?>
        <section class="institucional">
        </section>
    <?php
}
add_shortcode('page_institucional', 'page_institucional_shortcode');


/**
 * Últimas do blog - Home
 */
function ultimas_do_blog(){
    $posts = new WP_Query([
        'post_type' => 'post',
        'posts_per_page' => 3,
        'orderby' => 'date',
    ]);
    ?>
    <div class="ultimas_do_blog-card">
        <h1>Últimas do Blog</h1>
        <ul id="ultimas_do_blog" class="cS-hidden">
            <?php $count = 1;
            while ($posts->have_posts()): $posts->the_post();
                    $id = get_the_ID();
                    $title = get_the_title();
                    $position = $count % 2 === 0 ? 'even' : 'odd';
                ?>
                <li>
                    <article class="<?= $position ?>">
                        <a
                            class="<?= $position ?>"
                            href="<?= get_permalink($id) ?>"
                            alt="<?= $title ?>"
                            title="<?= $title ?>"
                        >
                            <img
                                src="<?= get_the_post_thumbnail_url($id, 'large') ?>"
                                alt="<?= $title ?>"
                                title="<?= $title ?>"
                            >
                        </a>
                        <div class="text-content <?= $position ?>">
                            <a
                                href="<?= get_permalink($id) ?>"
                                alt="<?= $title ?>"
                                title="<?= $title ?>"
                            >
                                <h2><?= $title ?></h2>
                            </a>
                            <p><?= get_the_excerpt() ?></p>
                        </div>
                    </article>
                </li>
            <?php $count ++; endwhile ?>
        </ul>
    </div>
<?php }
add_shortcode('ultimas_do_blog', 'ultimas_do_blog');
