<?php get_header(); ?>
    <section class="container" role="main">
        <?php if (have_posts()) : ?>
            <header class="entry-title">
                <h1>Resultados para: <span><?php the_search_query(); ?></span></h1>
            </header>

            <?php get_template_part('loop', 'search'); ?>
        <?php else : ?>
            <header class="entry-title">
                <h1>Nada encontrado</h1>
            </header>

            <div class="entry-content">
                <p>Desculpe, mas nada foi encontrado para sua busca</p>
            </div>
        <?php endif; ?>
    </section>

<?php get_footer(); ?>
