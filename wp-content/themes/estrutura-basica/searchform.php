<form id="search-form" role="search" method="get" action="<?php echo home_url('/'); ?>">
    <label for="s">
        <input type="search" value="" name="s" id="s" placeholder="Faça uma busca" />
    </label>
    <button id="search-submit">Buscar</button>
</form>