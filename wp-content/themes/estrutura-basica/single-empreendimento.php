<?php get_header() ?>

    <!-- Banner -->
    <?php if (!empty(get_field('banner', get_queried_object_id())['url'])): ?>
        <img
            src="<?= get_field('banner', get_queried_object_id())['url'] ?>"
            alt="Banner Destaque"
            title="Banner Destaque"
            class="banner-single-empreendimento"
        >
    <?php endif ?>
    <!-- Single Empreendimento -->
    <section class="single-empreendimento">
        <?php while (have_posts()): the_post() ?>
            <div class="small-container">
                <!-- Brand -->
                <div class="brand">
                    <div>
                        <h1><?= get_the_title() ?></h1>
                        <?php if (!empty(get_field('valores_a_partir_de'))): ?>
                            <p class="label-valor">
                                A partir de:
                                <br>
                                <span>
                                    <i>R$ <?= get_field('valores_a_partir_de') ?></i>
                                </span>
                            </p>
                        <?php endif ?>
                    </div>
                    <h2><?= get_the_terms(get_the_ID(), 'cidade')[0]->name ?></h2>
                    <p><?= the_content() ?></p>
                </div>
                <?php if (get_field('passeio_virtual')): ?>
                    <div class="brand">
                        <div>
                            <h1>Passeio Virtual</h1>
                        </div>
                        <div class="passeio-icon">
                            <a target="_blank" href="<?= get_field('passeio_virtual') ?>"><img src="/wp-content/themes/estrutura-basica/assets/images/predio-comercial.png">Clique aqui!</a>
                        </div>
                    </div>
                <?php endif; ?>
                <!-- Sobre o Imóvel -->
                <div class="sobre">
                    <h2>Conheça mais sobre<br>este Imóvel</h2>
                    <div class="infos">
                        <?php if (!empty(get_field('conheca_mais')['total_de_aptos'])): ?>
                            <p>Total de aptos: <?= get_field('conheca_mais')['total_de_aptos'] ?></p>
                        <?php endif ?>
                        <?php if (!empty(get_field('conheca_mais')['quantidade_de_blocos'])): ?>
                            <p>
                                <?= get_field('conheca_mais')['quantidade_de_blocos'] ?>
                                <?= (get_field('conheca_mais')['quantidade_de_blocos'] > 1) ? "blocos" : "bloco" ?>    
                            </p>
                        <?php endif ?>
                        <?php if (!empty(get_field('conheca_mais')['quantidade_saloes_festa'])): ?>
                            <p>
                                <?= get_field('conheca_mais')['quantidade_saloes_festa'] ?>
                                <?= (get_field('conheca_mais')['quantidade_saloes_festa'] > 1) ? "salões de festas" : "salão de festas" ?>
                            </p>
                        <?php endif ?>
                        <?php if (!empty(get_field('conheca_mais')['vaga_de_estacionamento'][0])): ?>
                            <p>Vaga de estacionamento: <?= get_field('conheca_mais')['vaga_de_estacionamento'][0] ?></p>
                        <?php endif ?>
                        <?php if(is_array(get_field('conheca_mais')['mais_informacoes'])): ?>
                            <?php foreach (get_field('conheca_mais')['mais_informacoes'] as $info): ?>
                                <?php if (!empty($info)): ?>
                                    <p><?= $info['informacao'] ?></p>
                                <?php endif ?>
                            <?php endforeach ?>
                        <?php else: ?>
                            <?php if (!empty(get_field('conheca_mais')['mais_informacoes'])): ?>
                                <p><?= get_field('conheca_mais')['mais_informacoes']['informacao'] ?></p>
                            <?php endif ?>
                        <?php endif ?>
                    </div>
                </div>
                <!-- Galleries -->
                <div class="gallery">
                    <ul id="single-empreendimento-slider-01">
                        <?php foreach (get_field('galeria') as $image): ?>
                            <li
                                data-thumb="<?= $image['sizes']['large'] ?>"
                                data-src="<?= $image['sizes']['large'] ?>"
                            >
                                <img
                                    src="<?= $image['sizes']['large'] ?>"
                                    alt="Imagem Imóvel"
                                    title="Imagem Imóvel"
                                />
                            </li>
                        <?php endforeach ?>
                    </ul>
                </div>
                <!-- Plantas -->
                <?php if (have_rows('plantas')): ?>
                    <div class="plantas">
                        <h2>Veja a planta deste<br>empreendimento</h2>
                        <ul id="single-empreendimento-slider-plantas">
                            <?php $count = 1 ?>
                            <?php while (have_rows('plantas')): the_row() ?>
                                <li>
                                    <div class="item">
                                        <img
                                            src="<?= get_sub_field('planta')['sizes']['large'] ?>"
                                            alt="Planta Imóvel"
                                            title="Planta Imóvel"
                                        >
                                        <div class="infos">
                                            <h4>Planta <?= $count ?></h4>
                                            <?php if (!empty(get_sub_field('area_total'))): ?>
                                                <p>
                                                    <i class="fas fa-ruler-combined"></i>
                                                    Área total: <?= get_sub_field('area_total') ?> m²
                                                </p>
                                            <?php endif ?>
                                            <?php if (!empty(get_sub_field('dormitorios'))): ?>
                                                <p>
                                                    <i class="fas fa-bed"></i>
                                                    <?= get_sub_field('dormitorios') ?> Dormitórios
                                                </p>
                                            <?php endif ?>
                                            <?php if (!empty(get_sub_field('mais_informacoes'))): ?>
                                                <?php foreach (get_sub_field('mais_informacoes') as $info): ?>
                                                    <?php if (!empty($info)): ?>
                                                        <p><?= $info['informacao'] ?></p>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </li>
                            <?php $count ++; endwhile ?>
                        </ul>
                    </div>
                <?php endif ?>
            </div>
            <!-- CTA -->
            <div class="cta">
                <div class="small-container">
                    <?= do_shortcode('[page_contato]') ?>
                </div>
            </div>
            <div class="small-container">
                <!-- Status -->
                <div class="status">
                    <h2>Fique ligado no Status<br>desta Obra</h2>
                    <div class="datas">
                        <?php if (!empty(get_field('status_da_obra')['inicio'])): ?>
                            <p>Início em <?= get_field('status_da_obra')['inicio'] ?></p>
                        <?php endif ?>
                        <?php if (!empty(get_field('status_da_obra')['termino'])): ?>
                            <p>Término em <?= get_field('status_da_obra')['termino'] ?></p>
                        <?php endif ?>
                    </div>
                    <div class="infos">
                        <div id="circle-escavacao">
                            <img
                                src="<?= get_image_url('single-empreendimento/escavacao.png') ?>"
                                alt="Ícone Escavação"
                                title="Escavação"
                            >
                            <p>Escavação<br><?= empty(get_field('status_da_obra')['escavacao']) ? 0 : get_field('status_da_obra')['escavacao'] ?>%</p>
                            <input
                                type="hidden"
                                class="escavacao"
                                value="<?= empty(get_field('status_da_obra')['escavacao']) ? 0 : get_field('status_da_obra')['escavacao'] ?>"
                            >
                        </div>
                        <div id="circle-fundacao">
                            <img
                                src="<?= get_image_url('single-empreendimento/fundacao.png') ?>"
                                alt="Ícone Fundação"
                                title="Fundação"
                            >
                            <p>Fundação<br><?= empty(get_field('status_da_obra')['fundacao']) ? 0 : get_field('status_da_obra')['fundacao'] ?>%</p>
                            <input
                                type="hidden"
                                class="fundacao"
                                value="<?= empty(get_field('status_da_obra')['fundacao']) ? 0 : get_field('status_da_obra')['fundacao'] ?>"
                            >
                        </div>
                        <div id="circle-estrutura">
                            <img
                                src="<?= get_image_url('single-empreendimento/estrutura.png') ?>"
                                alt="Ícone Estrutura"
                                title="Estrutura"
                            >
                            <p>Estrutura<br><?= empty(get_field('status_da_obra')['estrutura']) ? 0 : get_field('status_da_obra')['estrutura'] ?>%</p>
                            <input
                                type="hidden"
                                class="estrutura"
                                value="<?= empty(get_field('status_da_obra')['estrutura']) ? 0 : get_field('status_da_obra')['estrutura'] ?>"
                            >
                        </div>
                        <div id="circle-alvenaria">
                            <img
                                src="<?= get_image_url('single-empreendimento/alvenaria.png') ?>"
                                alt="Ícone Alvenaria"
                                title="Alvenaria"
                            >
                            <p>Alvenaria<br><?= empty(get_field('status_da_obra')['alvenaria']) ? 0 : get_field('status_da_obra')['alvenaria'] ?>%</p>
                            <input
                                type="hidden"
                                class="alvenaria"
                                value="<?= empty(get_field('status_da_obra')['alvenaria']) ? 0 : get_field('status_da_obra')['alvenaria'] ?>"
                            >
                        </div>
                        <div id="circle-acab_externo">
                            <img
                                src="<?= get_image_url('single-empreendimento/acabexterno.png') ?>"
                                alt="Ícone Acab. Externo"
                                title="Acab. Externo"
                            >
                            <p>Acab. Externo<br><?= empty(get_field('status_da_obra')['acab_externo']) ? 0 : get_field('status_da_obra')['acab_externo'] ?>%</p>
                            <input
                                type="hidden"
                                class="acab_externo"
                                value="<?= empty(get_field('status_da_obra')['acab_externo']) ? 0 : get_field('status_da_obra')['acab_externo'] ?>"
                            >
                        </div>
                        <div id="circle-acab_interno">
                            <img
                                src="<?= get_image_url('single-empreendimento/acabinterno.png') ?>"
                                alt="Ícone Acab. Interno"
                                title="Acab. Interno"
                            >
                            <p>Acab. Interno<br><?= empty(get_field('status_da_obra')['acab_interno']) ? 0 : get_field('status_da_obra')['acab_interno'] ?>%</p>
                            <input
                                type="hidden"
                                class="acab_interno"
                                value="<?= empty(get_field('status_da_obra')['acab_interno']) ? 0 : get_field('status_da_obra')['acab_interno'] ?>"
                            >
                        </div>
                    </div>
                </div>
                <!-- Localização -->
                <?php if (!empty($location = get_field('localizacao'))): ?>
                    <div class="localizacao">
                        <h2>Esta é a localização<br>deste empreendimento</h2>
                        <input class="lat" type="hidden" value="<?= $location['lat'] ?>">
                        <input class="lng" type="hidden" value="<?= $location['lng'] ?>">
                        <div>
                            <img
                                src="<?= get_image_url('placeholder-on-a-map.svg') ?>"
                                alt="Localizacao"
                                title="Localização"
                            >
                            <address><?= $location['address'] ?></address>
                        </div>
                        <section id="cd-google-map">
                            <div id="google-container"></div>
                            <div id="cd-zoom-in"></div>
                            <div id="cd-zoom-out"></div>
                        </section>
                    </div>
                <?php endif ?>
            </div>
        <script>
            <?= "document.getElementById('empreendimento').value = '".get_the_title()."'" ?>
        </script>
        <?php endwhile ?>
    </section>
<?php get_footer() ?>
