<?php get_header() ?>

    <section class="single-blog small-container">
        <?php while (have_posts()): the_post() ?>
            <?php
                $id = get_the_ID();
                $title = get_the_title();
            ?>
            <img
                src="<?= get_the_post_thumbnail_url($id, 'large') ?>"
                alt="<?= $title ?>"
                title="<?= $title ?>"
            >
            <div class="text-content">
                <h1><?= $title ?></h1>
                <?= the_content() ?>
            </div>
        <?php endwhile ?>
    </section>

<?php get_footer() ?>
