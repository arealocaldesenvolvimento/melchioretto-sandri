<?php
	/*Template name: Home */
get_header() ?>
	<article id="id-<?php the_ID() ?>" <?php post_class() ?>>
		<div class="entry-content">
			<div class="banner">
				<?= do_shortcode('[rev_slider alias="home-1"][/rev_slider]') ?>
			</div>
			<div class="container">
				<?= do_shortcode('[empreendimentos]') ?>
			</div>
			<?= do_shortcode('[counter]') ?>
			<div class="container">
				<?= do_shortcode('[ultimas_do_blog]') ?>
			</div>
		</div>
	</article>
<?php get_footer() ?>
