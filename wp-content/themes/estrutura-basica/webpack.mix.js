/**
 * Modules
 */
const mix = require('laravel-mix')

/**
 * Project proxy
 */
const proxy = 'http://localhost/your-site-dir'

mix
	/**
	 * SCSS/SASS transpiler
	 */
	.sass('assets/css/scss/estilo.scss', 'assets/css/style.css')
	/**
     * CSS
     */
	.styles(['assets/css/libs/*.css'], 'assets/css/libs.css')
	.styles(['assets/css/*.css'], 'style.css')
	/**
	 * JS transpiler
	.scripts(['assets/js/libs/*.js'], 'public/js/vendor.js')
	/**
	 * JS watcher
	 */
	.js(['assets/js/funcoes/funcoes.js'], 'assets/js/funcoes.min.js')
	/**
	 * Configs
	 */
	.options({ processCssUrls: false })
	.disableNotifications()
	.setPublicPath('')
	.webpackConfig({
		output: {
			chunkFilename: 'public/chunks/[name].js',
		},
	})
